# metalsmith-add-orig-file-name

Metalsmith middleware to add an `originalFilename` property to files in the event that other middleware removes it
