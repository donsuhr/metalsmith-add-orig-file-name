const addOriginalFilename = function addOriginalFilename(config) {
    return (files, metalsmith, done) => {
        Object.keys(files).forEach((file) => {
            files[file].originalFilename = file;
        });
        done();
    };
};

module.exports = addOriginalFilename;
